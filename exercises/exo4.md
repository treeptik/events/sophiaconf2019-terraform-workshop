# Exercice 4 : Modification de l'infrastructure

Dans cet exercice nous allons ajouter une seconde VM sur le même modèle que la première.

Ajouter la nouvelle variable `VM_NUMBER` dans le fichier de variables avec `2` comme valeur par défaut.

Dans le fichier `vm.tf`, ajouter la propriété count à la ressource en utilisant la variable `VM_NUMBER` comme valeur.

Utiliser aussi l'iterateur du *count* pour rendre dynamique le nom des VMs, pour ce faire remplacer la propriété `name` comme ceci :

```yaml
# Nom statique
name = "${var.STUDENT_NAME}-1"
# Nom dynamique
name = "${var.STUDENT_NAME}-${count.index + 1}"
```

Vérifier avec la planification que :

- Une ressource de type `digitalocean_droplet` va bien être ajoutée
- Son nom dynamique est bien `studentX-2`
- Aucune autre ressource ne va être supprimée ou modifiée

Appliquer les modifications.

Récupérer l'IP de cette nouvelle VM et vérifier qu'il est possible de s'y connecter en SSH avec le compte root.
