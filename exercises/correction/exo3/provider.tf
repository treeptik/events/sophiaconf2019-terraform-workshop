# Configure the DigitalOcean Provider
# DO token is taken from DIGITALOCEAN_TOKEN environment variable
provider "digitalocean" {
  version = "~> 1.4.0"
}