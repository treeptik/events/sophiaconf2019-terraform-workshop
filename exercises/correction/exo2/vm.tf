# Create Droplet master for each student
resource "digitalocean_droplet" "vm" {
  name     = "studentX-1"
  image    = "ubuntu-18-04-x64"
  region   = "fra1"
  size     = "s-1vcpu-1gb"
  ssh_keys = ["a2:91:28:c9:65:9b:b3:68:5b:f7:82:f8:c4:b4:af:97"]
}