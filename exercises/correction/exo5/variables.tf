variable "REGION" {
  default = "fra1"
}

variable "VM_IMAGE" {
  default = "ubuntu-18-04-x64"
}

variable "VM_SIZE" {
  default = "s-1vcpu-1gb"
}

variable "VM_SSH_KEYS" {
  default = "a2:91:28:c9:65:9b:b3:68:5b:f7:82:f8:c4:b4:af:97"
}

variable "STUDENT_NAME" {
  default = "studentX"
}

variable "VM_NUMBER" {
  default = 2
}
