# SophiaConf 2019 - Workshop Terraform

## Description du Workshop

L'objectif de ce workshop est de vous permettre de découvrir les fonctionnalités principales de [Terraform](https://www.terraform.io), outil incontournable dans le domaine de l'Infrastructure as Code.

Nous manipulerons ensemble sur des exercices simples et vous comprendrez rapidement pourquoi cet outil est tant apprécié !

### Prérequis

- Un ordinateur
- Un client SSH
- Une connexion Internet avec le droit de sortir en SSH

### Supports

- [Support de présentation PDF](./pdf/SophiaConf2019_Workshop_Terraform.pdf)
- [Support de présentation HTML](./slides/index.html) <- A lancer dans un navigateur
- [Exercices](./exercises/README.md)

### Export PDF

Lancer la présentation dans Chrome en ajoutant `?print-pdf` à la fin de l'URL puis :

- orientation paysage
- aucune marge

## Auteur

<img src="./img/Thomas_Perelle.jpg"  width="120">

Thomas Perelle

### Biographie

Dans le cadre de mes expériences précédentes j’ai eu l’occasion de faire du développement, de l’intégration, de l’administration système, de la gestion de projet et du management. Cela m’a permi d’acquérir une vision globale sur les métiers de l’IT et sur la chaîne complète, de la collecte du besoin jusqu’à la mise en production.

En parallèle je me suis toujours intéressé aux aspects méthodologique et à l’outillage qui permettrait de gagner en qualité, en productivité voire en confort ! C’est ce qui m’a naturellement amené à m’intéresser au méthodes agiles comme Scrum, Lean et à la mouvance DevOps.

Aujourd’hui je suis consultant sénior chez TREEPTIK et toutes ces expéricences me sont précieuses pour comprendre les besoins de mes clients et la meilleure manière de les accompagner dans l'atteinte de leurs objectifs stratégiques.
